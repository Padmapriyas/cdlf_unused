import { Component, OnInit } from '@angular/core';
declare let google: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawCorpDebtMarket);
        google.charts.setOnLoadCallback(drawMdlMarketLoanIndChart);
        google.charts.setOnLoadCallback(drawMdlMarketLoanCreditChart);
        google.charts.setOnLoadCallback(drawCdliIncReturnChart);
        google.charts.setOnLoadCallback(drawYieldCompChart);
        google.charts.setOnLoadCallback(drawYieldByCreditCompChart);
        google.charts.setOnLoadCallback(netRealGainChart);
        google.charts.setOnLoadCallback(netUnRealGainChart);
        google.charts.setOnLoadCallback(drawCdliTotRetChart);
        //google.charts.setOnLoadCallback(drawAvailRiskPremChart);


        //CDLI Total Return (trailing 4 quarter)
        function drawAvailRiskPremChart() {
          var data = google.visualization.arrayToDataTable([
            ['Middle Market Loans','Broadly Syndicated Loans','Directly Originated - Upper Middle Market', 'Non-Sponsor Borrowers', 'Lower Middle Market', 'Second Lien - Subordinated Debt'],
            ['Broadly Syndicated Loans', 4.8, 0, 0, 0, 0],
            ['Directly Originated - Upper Middle Market', 0, 1.8, 0, 0, 0],
            ['Non-Sponsor Borrowers', 0, 0, 2.8,0,0]
          ]);
          var options = {
            title: 'Available Risk Premiums in Direct U.S. Middle Market Loans (as of Dec 2017)',
            curveType: 'function',
            seriesType: 'bars',
            vAxis: {title: "Current Yield (LTM)"},
            isStacked: true,
            //legend: { position: 'bottom' }
          };
          var chart = new google.visualization.ColumnChart(document.getElementById('avlRiskPremChart'));
          chart.draw(data, options);
      }


        //CDLI Total Return (trailing 4 quarter)
        function drawCdliTotRetChart() {
          var data = google.visualization.arrayToDataTable([
            ['Month', 'Trailing 4 Quarters Total Return', 'Annualized Total Return'],
            ["Sep-05", 9.70, 9.70],
            ["Dec-05", 9.70, 10],
            ["Mar-06", 9.70, 11],
            ["Jun-06", 9.70, 12.5],
            ["Sep-06", 9.70, 13],
            ["Dec-06", 9.70, 13.2],
            ["Mar-07", 9.70, 14.5],
            ["Jun-07", 9.70,  15.2],
            ["Sep-07", 9.70, 12.5],
            ["Dec-07", 9.70, 10.5],
            ["Mar-08", 9.70, 4.5],
            ["Jun-08", 9.70, 3.5],
            ["Sep-08", 9.70, 1.5],
            ["Dec-08", 9.70, -7],
            ["Mar-09", 9.70, -3.5],
            ["Jun-09", 9.70, -3.5],
            ["Sep-09", 9.70, 2],
            ["Dec-09", 9.70, 13.5],
            ["Mar-10", 9.70, 14.5],
            ["Jun-10", 9.70, 15.3],
            ["Sep-10", 9.70, 14.7],
            ["Dec-10", 9.70, 15.5],
            ["Mar-11", 9.70, 16],
            ["Jun-11", 9.70, 14.7],
            ["Sep-11", 9.70, 11],
            ["Dec-11", 9.70, 9.7],
            ["Mar-12", 9.70, 9.8],
            ["Jun-12", 9.70, 9.9],
            ["Sep-12", 9.70, 14.5],
            ["Dec-12", 9.70, 14.3],
            ["Mar-13", 9.70, 14],
            ["Jun-13", 9.70, 14.2],
            ["Sep-13", 9.70, 12.8],
            ["Dec-13", 9.70, 12.8],
            ["Mar-14", 9.70, 12.6],
            ["Jun-14", 9.70, 12.7],
            ["Sep-14", 9.70, 11.7],
            ["Dec-14", 9.70, 9.7],
            ["Mar-15", 9.70, 9.5],
            ["Jun-15", 9.70, 9.3],
            ["Sep-15", 9.70, 9],
            ["Dec-15", 9.70, 7.5],
            ["Jun-16", 9.70, 6],
            ["Sep-16", 9.70, 5.5],
            ["Dec-16", 9.70, 5.7],
            ["Mar-17", 9.70, 7.3],
            ["Jun-17", 9.70, 11.5],
            ["Sep-17", 9.70, 13],
            ["Dec-17", 9.70, 10.5],
            ["Mar-18", 9.70, 9.3],
            ["Jun-18", 9.70, 8.62]
          ]);
          var options = {
            title: 'Annual CDLI total returns are fairly stable, but for the Financial Crisis',
            curveType: 'function',
            seriesType: 'bars',
            series: {0: {type: 'line'}},
            hAxis: {title: "Month"},
            vAxis: {title: "CDLI Total Return"}
            //legend: { position: 'bottom' }
          };
          var chart = new google.visualization.ComboChart(document.getElementById('cdliTotRetChart'));
          chart.draw(data, options);
      }

        //CDLI Net UnRealized Gains (Losses)
        function netUnRealGainChart() {
          var data = google.visualization.arrayToDataTable([
            ['Month', 'Annualized Net Unrealized Gains (Losses)', 'Trailing 4 Quarters Net Unrealized Gains (Losses)', 'Cumulative Net Unrealized Gains (Losses)'],
            ["Sep-05", -0.33, 0.5, 0.5],
            ["Dec-05", -0.33, 0, 0.2],
            ["Mar-06", -0.33, 1.5, 1.5],
            ["Jun-06", -0.33, 2, 1.9],
            ["Sep-06", -0.33, 1.9, 2],
            ["Dec-06", -0.33, 1.8, 2.5],
            ["Mar-07", -0.33, 1.5, 2.4 ],
            ["Jun-07", -0.33, 2.3, 4],
            ["Sep-07", -0.33, 0.5, 1.9],
            ["Dec-07", -0.33, -2, 0],
            ["Mar-08", -0.33, -6.5, -4],
            ["Jun-08", -0.33, -7.5, -4],
            ["Sep-08", -0.33, -9, -9],
            ["Dec-08", -0.33, -16, -16],
            ["Mar-09", -0.33, -13.5, -16.2],
            ["Jun-09", -0.33, -12, -14],
            ["Sep-09", -0.33, -6.5, -13.5],
            ["Dec-09", -0.33, 8, -7],
            ["Mar-10", -0.33, 12, -7.5],
            ["Jun-10", -0.33, 9, -8],
            ["Sep-10", -0.33, 8, -4],
            ["Dec-10", -0.33, 6.5, -3.5],
            ["Mar-11", -0.33, 3.5, -3.7],
            ["Jun-11", -0.33, 4, -3.9],
            ["Sep-11", -0.33, 0, -7],
            ["Dec-11", -0.33, -0.4, -4],
            ["Mar-12", -0.33, -0.4, -3.7],
            ["Jun-12", -0.33, 0, -3.3],
            ["Sep-12", -0.33, 3.5, -2.5],
            ["Dec-12", -0.33, 2.5, -2.4],
            ["Mar-13", -0.33, 2.3, -2.2],
            ["Jun-13", -0.33, 2.1, -2.2],
            ["Sep-13", -0.33, 1.2, -1.8],
            ["Dec-13", -0.33, 1.9, -1.6],
            ["Mar-14", -0.33, 1.8, -1.6],
            ["Jun-14", -0.33, 1.9, -1.6],
            ["Sep-14", -0.33, 0.9, -1.9],
            ["Dec-14", -0.33, -1.9, -2.5],
            ["Mar-15", -0.33, -2, -2.5],
            ["Jun-15", -0.33, -2, -2.5],
            ["Sep-15", -0.33, -2.5, -3.5],
            ["Dec-15", -0.33, -3.6, -7],
            ["Jun-16", -0.33, -4.5, -7.5],
            ["Sep-16", -0.33, -4, -6.5],
            ["Dec-16", -0.33, -1, -4.5],
            ["Mar-17", -0.33, 2.5, -4.4],
            ["Jun-17", -0.33, 3.5, -4.3],
            ["Sep-17", -0.33, 3.5, -4.1],
            ["Dec-17", -0.33, 1, -4.2],
            ["Mar-18", -0.33, 0.5, -4.23]
          ]);
          var options = {
            title: 'Net unrealized gains (losses) telegraph changes in general credit spreads or anticipated realized losses',
            curveType: 'function',
            seriesType: 'line',
            series: {1: {type: 'bars'}},
            hAxis: {title: "Month"},
            vAxis: {title: "Net Unrealized Losses as % of Average Assets"}
            //legend: { position: 'bottom' }
          };
          var chart = new google.visualization.ComboChart(document.getElementById('netUnRealGainChart'));
          chart.draw(data, options);
      }

        //CDLI Net Realized Gains (Losses)
        function netRealGainChart() {
          var data = google.visualization.arrayToDataTable([
            ['Month', 'Annualized Net Realized Gains (Losses)', 'Trailing 4 Quarters Net Realized Gains (Losses)', 'Cumulative Net Realized Gains (Losses)'],
            ["Sep-05", -1.05, 1, 1],
            ["Dec-05", -1.05, 1, 1],
            ["Mar-06", -1.05, 1, 1],
            ["Jun-06", -1.05, 0.2, 0.9],
            ["Sep-06", -1.05, 0.1, 1.5],
            ["Dec-06", -1.05, 0.8, 2],
            ["Mar-07", -1.05, 0.8, 1.9],
            ["Jun-07", -1.05, 2.2, 2.5],
            ["Sep-07", -1.05, 2, 2.4],
            ["Dec-07", -1.05, 1.3, 2.5],
            ["Mar-08", -1.05, 2.1, 3],
            ["Jun-08", -1.05, 0.2, 3],
            ["Sep-08", -1.05, 0.2, 2.8],
            ["Dec-08", -1.05, 0, 2.7],
            ["Mar-09", -1.05, -0.5, 2.65],
            ["Jun-09", -1.05, -0.9, 2.6],
            ["Sep-09", -1.05, -3.5, 0],
            ["Dec-09", -1.05, -4, -1.05],
            ["Mar-10", -1.05, -7.5, -4.9],
            ["Jun-10", -1.05, -9, -6.5],
            ["Sep-10", -1.05, -5.5, -5.5],
            ["Dec-10", -1.05, -5, -6],
            ["Mar-11", -1.05, -2.5, -7.5],
            ["Jun-11", -1.05, 0, -7.5],
            ["Sep-11", -1.05, -2, -7.6],
            ["Dec-11", -1.05, -1.05, -7.8],
            ["Mar-12", -1.05, -1.9, -8.5],
            ["Jun-12", -1.05, -1.9, -8.5],
            ["Sep-12", -1.05, -2.2, -8.7],
            ["Dec-12", -1.05, -2.1, -8.7 ],
            ["Mar-13", -1.05, -0.5, -8.7],
            ["Jun-13", -1.05, -0.7, -8.6],
            ["Sep-13", -1.05, -0.2, -8.8],
            ["Dec-13", -1.05, -0.1, -8.8],
            ["Mar-14", -1.05, -0.2, -8.8],
            ["Jun-14", -1.05, -0.2, -8.7],
            ["Sep-14", -1.05, 0.1, -8.6],
            ["Dec-14", -1.05, 0.2, -8.6],
            ["Mar-15", -1.05, 0.1, -8.8],
            ["Jun-15", -1.05, 0, -8.6],
            ["Sep-15", -1.05, -0.2, -8.9],
            ["Dec-15", -1.05, -0.8, -9.5],
            ["Jun-16", -1.05, -0.7, -9.8],
            ["Sep-16", -1.05, -1, -9.9],
            ["Dec-16", -1.05, -1.05, -10],
            ["Mar-17", -1.05,  -1.05, -10.5],
            ["Jun-17", -1.05, -1.2, -12],
            ["Sep-17", -1.05, -1.1, -12],
            ["Dec-17", -1.05, -1.2, -13.07]
          ]);
          var options = {
            title: 'Net realized losses contributed about −1.05% per year, roughly equal to realized losses for broadly syndicated bank loans and below a -1.4% loss ratio for high yield bonds',
            curveType: 'function',
            seriesType: 'line',
            series: {1: {type: 'bars'}},
            hAxis: {title: "Month"},
            vAxis: {title: "Net Realized Losses as % of Average Assets"}
            //legend: { position: 'bottom' }
          };
          var chart = new google.visualization.ComboChart(document.getElementById('netRealGainChart'));
          chart.draw(data, options);
      }

        function drawYieldByCreditCompChart() {
        var data = google.visualization.arrayToDataTable([
          ['Month', 'True Senior', 'Senior', 'CDLI', 'Subordinated'],
          ['Mar-13', 9, 11, 12.5, 14],
          ['Jun-13', 8.9, 10.2, 12, 14.5],
          ['Sep-13', 8.8, 9.9, 11.9, 15.2],
          ['Dec-13', 8, 9.5, 11.7, 15.5],
          ['Mar-14', 7.8, 9.3, 11.7, 15.7],
          ['Jun-14', 7.6, 9.5, 11.5, 15],
          ['Sep-14', 7.5, 9.7, 11.3, 14.1],
          ['Dec-14', 7.9, 9.8, 11.3, 13.8],
          ['Mar-15', 7.9, 10, 11, 12.2],
          ['Jun-15', 7.9, 10, 11, 11.9],
          ['Sep-15', 7.8, 9.9, 10.9, 12],
          ['Dec-15', 7.7, 9.8, 10.9, 12.1],
          ['Mar-16', 7.7, 9.7, 10.9, 12.3],
          ['Jun-16', 7.8, 9.9, 10.8, 12.1],
          ['Sep-16', 8, 9.9, 10.8, 12.2],
          ['Dec-16', 7.8, 9.8, 10.7, 12.3],
          ['Mar-17', 7.6, 9.8, 10.6, 12.1],
          ['Jun-17', 7.6, 9.7, 10.5, 12],
          ['Sep-17', 7.3, 9.7, 10.2, 11.6],
          ['Dec-17', 7.4, 9.7, 10.2, 11.4]
        ]);
        var options = {
          title: 'True Senior, Senior, Subordinated L4Q Yields Mar 2013 through Dec 2017',
          curveType: 'function',
          hAxis: {title: "Month"},
          vAxis: {title: "Trailing 4 Quarter Current Yield Estimates"}
          //legend: { position: 'bottom' }
        };
        var chart = new google.visualization.LineChart(document.getElementById('yieldByCreditChart'));
        chart.draw(data, options);
      }

        //Yield Comparison
        function drawYieldCompChart() {
          var data = google.visualization.arrayToDataTable([
            ['Month', 'Spread between CDLI and HY Bonds', 'CDLI (3Yr Takeout Yield)', 'Bloomberg Barclays High Yield Bond Index (YTW)'],
            ['Sep-04',  -1, 6.5, 7],
            ['Mar-05',  0, 6, 6.7],
            ['Sep-05',  2,  6.7, 6.7],
            ['Mar-06',  1.3, 10, 7],
            ['Sep-06',  2.5,  10.2, 8],
            ['Mar-07',  2.2, 9.8, 6.9],
            ['Sep-07',  1.3, 9.9, 7.2],
            ['Mar-08',  1.4, 11.5, 10.5],
            ['Sep-08',  2, 12.5, 10.5],
            ['Mar-09',  2.5, 20.5, 19.5],
            ['Sep-09',  8, 21.5, 18.5],
            ['Mar-10',  6, 15, 8],
            ['Sep-10',  5.5,  15.2, 9.5],
            ['Mar-11',  5, 15.3, 9.3],
            ['Sep-11',  4, 12.5, 7.5],
            ['Mar-12',  5, 13, 9.5],
            ['Sep-12',  4.9, 12.5, 7.5],
            ['Mar-13',  4.8, 11, 6],
            ['Sep-13',  4.8,11.3,6.2],
            ['Mar-14',  4.6, 10, 5.5],
            ['Sep-14',  4.4, 10, 5.3],
            ['Mar-15',  4.0, 11, 6.3],
            ['Sep-15',  3, 11, 6.5],
            ['Mar-16',  4.0, 12.3, 9],
            ['Sep-16',  5, 11.2, 6],
            ['Mar-17',  5, 10.5,5.5],
            ['Sep-17',  5.19, 10.91, 5.72]
          ]);
          var options = {
            title: 'Yield spread to high yield bonds averages 4.58% over the past 10 years, and equals 5.19% at December 2017',
            curveType: 'function',
            seriesType: 'line',
            series: {0: {type: 'area'}},
            hAxis: {title: "Month"},
            vAxis: {title: "Yield-to-Maturity (%)"}
            //legend: { position: 'bottom' }
          };
          var chart = new google.visualization.ComboChart(document.getElementById('yieldCompChart'));
          chart.draw(data, options);
      }

        //Private direct lending provides 4% of U.S. corporate debt financing
        function drawCorpDebtMarket() {
            let data = google.visualization.arrayToDataTable([
                ['Task', 'Hours per Day'],
                ['Investment Grade', 5.2],
                ['High Yield Corporate', 1.3],
                ['High Yield Syndicated', 1.0],
                ['Bank Commercial & Industrial Loans', 2.1],
                ['Direct Lending - Government', 0.6],
                ['Direct Lending - Private Middle Market', 0.4]
            ]);
            let options = {
                title: '$10.6 Trillion U.S. Corporate Debt Market',
                is3D: true
            };
            var chart = new google.visualization.PieChart(document.getElementById('corpDebtMarket'))
            chart.draw(data, options);
        }

        //Middle Market Direct Loans by Industry
         function drawMdlMarketLoanIndChart() {
            let data = google.visualization.arrayToDataTable([
                ['Industries', 'Loans in %'],
                ['Services', 30],
                ['Manufacturing', 20],
                ['Telecom & Utilities', 7],
                ['Retail', 5],
                ['Construction', 1],
                ['Financials', 21],
                ['Energy', 7],
                ['Wholesale', 6],
                ['Mining', 2],
                ['Public Adm', 1]
            ]);
            let options = {
                title: 'Middle Market Direct Loans by Industry (as of Sep 2017)',
                is3D: true
            };
            var chart = new google.visualization.PieChart(document.getElementById('mdlMarketLoanInd'))
            chart.draw(data, options);
        }

         //Middle Market Direct Loans by Credit Type
         function drawMdlMarketLoanCreditChart() {
            let data = google.visualization.arrayToDataTable([
                ['Credit Type', 'Loans in %'],
                ['Senior Secured: First Lien', 51],
                ['Senior Secured: Second Lien', 18],
                ['Subordinated Bond/Note', 11],
                ['Equity', 8],
                ['Senior Secured: Unitranche', 2],
                ['Secured Debt: Senior Note', 4],
                ['Preferred Equity',3],
                ['Other', 2]
            ]);
            let options = {
                title: 'Middle Market Direct Loans by Credit Type (as of Sep 2017)',
                is3D: true
            };
            var chart = new google.visualization.PieChart(document.getElementById('mdlMarketLoanCredit'))
            chart.draw(data, options);
        }

        //CDLI income return (Sep 2004 to Dec 2017)
         function drawCdliIncReturnChart() {
           var data = google.visualization.arrayToDataTable([
            ["Date", "CDLI Income Return", "Incept.",{ role: "style" } ],
            ["Sep-05", 8, 11.15,"#b6b986"],
            ["Dec-05", 8.5, 11.15,"#b6b986"],
            ["Mar-06", 9, 11.15,"#b6b986"],
            ["Jun-06", 9.5, 11.15,"#b6b986"],
            ["Sep-06", 10.5, 11.15,"#b6b986"],
            ["Dec-06", 11, 11.15,"#b6b986"],
            ["Mar-07", 11.15, 11.15,"#b6b986"],
            ["Jun-07", 11.15,11.15, "#b6b986"],
            ["Sep-07", 10.3,11.15, "#b6b986"],
            ["Dec-07", 10.2, 11.15,"#b6b986"],
            ["Mar-08", 10.3,11.15, "#b6b986"],
            ["Jun-08", 10.3, 11.15,"#b6b986"],
            ["Sep-08", 11.8, 11.15,"#b6b986"],
            ["Dec-08", 12, 11.15,"#b6b986"],
            ["Mar-09", 12.5, 11.15,"#b6b986"],
            ["Jun-09", 13, 11.15,"#b6b986"],
            ["Sep-09", 13.2,11.15, "#b6b986"],
            ["Dec-09", 13.1,11.15, "#b6b986"],
            ["Mar-10", 13, 11.15,"#b6b986"],
            ["Jun-10", 12.9, 11.15,"#b6b986"],
            ["Sep-10", 12.9,11.15, "#b6b986"],
            ["Dec-10", 13,11.15, "#b6b986"],
            ["Mar-11", 13, 11.15,"#b6b986"],
            ["Jun-11", 12.9,11.15, "#b6b986"],
            ["Sep-11", 12.8,11.15, "#b6b986"],
            ["Dec-11", 12.7,11.15, "#b6b986"],
            ["Mar-12", 13, 11.15,"#b6b986"],
            ["Jun-12", 13.2,11.15, "#b6b986"],
            ["Sep-12", 13.3,11.15, "#b6b986"],
            ["Dec-12", 13.5, 11.15,"#b6b986"],
            ["Mar-13", 13.6,11.15, "#b6b986"],
            ["Jun-13", 12.8,11.15, "#b6b986"],
            ["Sep-13", 12.6,11.15, "#b6b986"],
            ["Dec-13", 12.2,11.15, "#b6b986"],
            ["Mar-14", 12, 11.15,"#b6b986"],
            ["Jun-14", 11.2,11.15, "#b6b986"],
            ["Sep-14", 11.15, 11.15,"#b6b986"],
            ["Dec-14", 11, 11.15,"#b6b986"],
            ["Mar-15", 10.8, 11.15,"#b6b986"],
            ["Jun-15", 10.8, 11.15,"#b6b986"],
            ["Sep-15", 10.7, 11.15,"#b6b986"],
            ["Dec-15", 10.8, 11.15,"#b6b986"],
            ["Jun-16", 10.7, 11.15,"#b6b986"],
            ["Sep-16", 10.6, 11.15,"#b6b986"],
            ["Dec-16", 10.5, 11.15,"#b6b986"],
            ["Mar-17", 10.4, 11.15,"#b6b986"],
            ["Jun-17", 10.3, 11.15,"#b6b986"],
            ["Sep-17", 10.16, 11.15,"#b6b986"],
            ["Dec-17", 10.16, 11.15,"#b6b986"]
          ]);

          /*var view = new google.visualization.DataView(data);
          view.setColumns([0, 1,
                           { calc: "stringify",
                             sourceColumn: 1,
                             type: "string",
                             role: "annotation" },
                           2]);*/

           var options = {
        title: "Interest and other income has ranged from 9% to 12% (11.15% annualized since inception)",
        legend: "none",
        chartArea: {
            width: 1200,
            left: 50
        },
        bar: {
            groupWidth: "80%"
        },
        tooltip: {
            isHtml: true
        },
        hAxis: {title: "Month"},
        vAxis: {title: "CDLI Income Return (%)"},
        color:"#b6b986",
        seriesType: 'bars',
        series: {1: {type: 'line'}}
        dataOpacity: 0.7
    };

    // Create and draw the visualization.
    new google.visualization.ComboChart(document.getElementById('cdliIncReturnChart')).draw(data, options);
        }
  }

}
